<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventaris', function (Blueprint $table) {
            $table->increments('id');
			$table->string('nama',100);
			$table->string('kode',6);
			$table->enum('kondisi',['baik','rusak']);
			$table->text('keterangan');
			$table->date('tanggal_register');
			$table->integer('stok');
			$table->integer('id_jenis');
			$table->integer('id_ruangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventaris');
    }
}
