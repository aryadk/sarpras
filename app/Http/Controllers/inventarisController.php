<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventaris;
use App\Jenis;
use App\Ruangan;

class inventarisController extends Controller
{
    function index(){
		$no = 1;
		$data = Inventaris::
		join('jenis','jenis.id','=','inventaris.id_jenis')
		->join('ruangan','ruangan.id','=','inventaris.id_ruangan')
		->select('inventaris.*','jenis.keterangan as jenis','ruangan.kode_ruang as ruangan')
		->get();
		return view('inventaris',compact('data','no'));
	}
	function tambahView(){
		$data = Jenis::get();
		$data1 = Ruangan::get();
		return view('tambah_inventaris',compact('data','data1'));
	}
	function tambah(Request $request){
		$data = new Inventaris;
		$data->kode = $request->kode;
		$data->nama = $request->nama_barang;
		$data->kondisi = $request->kondisi;
		$data->stok = $request->stok;
		$data->tanggal_register = date("Y-m-d");
		$data->keterangan = $request->keterangan;
		$data->id_jenis = $request->jenis;
		$data->id_ruangan = $request->ruangan;
		$data->save();
		return redirect('inventaris');
	}
	function tambah_stok($id){
		$data = Inventaris::where('id',$id)->first();
		$jumlah = $data->stok + 1;
		$update = Inventaris::where('id',$id)
		->update(['stok'=>$jumlah]);
		return redirect('inventaris');
	}
	function kurang_stok($id){
		$data = Inventaris::where('id',$id)->first();
		$jumlah = $data->stok - 1;
		$update = Inventaris::where('id',$id)
		->update(['stok'=>$jumlah]);
		return redirect('inventaris');
	}
	function editView($id){
		$data = Inventaris::where('id',$id)->first();
		$data1 = Jenis::get();
		$data2 = Ruangan::get();
		return view('edit_inventaris',compact('data','data1','data2'));
	}
	function edit($id,Request $request){
		$data = Inventaris::find($id);
		$data->kode = $request->kode;
		$data->nama = $request->nama_barang;
		$data->kondisi = $request->kondisi;
		$data->stok = $request->stok;
		$data->keterangan = $request->keterangan;
		$data->id_jenis = $request->jenis;
		$data->id_ruangan = $request->ruangan;
		$data->save();
		return redirect('inventaris');
	}
	function laporan(){
		$no = 1;
		$tgl= date('d-m-Y');
		$data = Inventaris::
		join('jenis','jenis.id','=','inventaris.id_jenis')
		->join('ruangan','ruangan.id','=','inventaris.id_ruangan')
		->select('inventaris.*','jenis.keterangan as jenis','ruangan.kode_ruang as ruangan')
		->get();
		return view('laporan',compact('data','no','tgl'));
	}
}
