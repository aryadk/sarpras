<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\DetailPeminjaman;
use App\Pegawai;
use App\Inventaris;

class peminjamanController extends Controller
{
    function index(){
		$no=1;
		$data = Peminjaman::
		join('detail_peminjaman','peminjaman.id','=','detail_peminjaman.id_peminjaman')
		->join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
		->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
		->select('peminjaman.*','pegawai.nip as nip','pegawai.nama as nama'
		,'inventaris.nama as nama_barang','detail_peminjaman.jumlah')
		->orderBy('peminjaman.id','desc')
		->get();
		return view('peminjaman',compact('data','no'));
	}
	function tambahView(){
		return view('tambah_peminjaman');
	}
	function tambah(Request $request){
		//1. memperkenalkan variabel dari request form tambah_peminjaman 
		$nip = $request->nip;
		$nama = $request->nama;
		$nama_barang = $request->nama_barang;
		$jumlah = $request->jumlah;
		
		//2. memanggil data id dari pegawai yang nip nya sama dengan variabel nip diatas (no 1)
		//data id ini akan digunakan untuk mengisi id_pegawai di no 3
		$dataPe = Pegawai::where('nip',$nip)->first();
		
		// sekarang kita menambahkan peminjaman dengan mengisi secara bersamaan tabel peminjaman dan detail
		//3. menambahkan data baru peminjaman dengan data-data yang diperoleh dari no 2
		if($dataPe == null ){
			return redirect('peminjaman')->with('alert','Peminjaman gagal, NIP tidak ditemukan');
		}
		$data1= new Peminjaman;
		$data1->tanggal_pinjam = date("Y-m-d");
		$data1->status = 'belum kembali';
		$data1->id_pegawai = $dataPe->id;
		$data1->save();
		
		//4. memanggil data id dari peminjaman yang terakhir diisi 
		//data ini untuk mengisi id_peminjaman di tabel detail  
		$dataP = Peminjaman::orderBy('id','desc')->first();
		
		//5. memanggil data id dari inventaris yang terakhir diisi, 
		//yang mana nama inventaris ini adalah nilai variabel nama_barang.
		//data ini untuk mengisi id_inventaris di tabel detail
		$dataI = Inventaris::
		where('nama',$nama_barang)
		->first();
		//6. menambahkan data baru detail_peminjaman dengan data-data yg sudah di peroleh dsri no 1,4,5
		if($dataI == null ){
			return redirect('peminjaman')->with('alert','Peminjaman gagal, Barang tidak ditemukan');
		}
		if($jumlah > $dataI->stok ){
			return redirect('peminjaman')->with('alert','Peminjaman gagal, Stok tidak cukup');
		}
		$data2= new DetailPeminjaman;
		$data2->jumlah = $request->jumlah;
		$data2->id_inventaris = $dataI->id;
		$data2->id_Peminjaman = $dataP->id;
		$data2->save();
		
		//7. membuat variabel stok akhir, fungsinya adalah sebagai wadah 
		//menyimpan nilai stok inventaris setelah dipinjam rumusnya stok -jumlah
		$stok_akhir = $dataI->stok - $jumlah;
		
		//8. merubah stok inventaris menjadi nilai variabel stok_akhir
		$update=Inventaris::find($dataI->id);
		$update->stok = $stok_akhir;
		$update->save();
		return redirect('peminjaman');
		
	}
	function sudah_kembali($id){
		//1. merubah status dan tanggal menjadi sudah kembali dan tanggal hari ini
		$data= Peminjaman::find($id);
		$data->status = 'sudah kembali';
		$data->tanggal_kembali = date("Y-m-d");
		$data->save();
		
		//2.memanggil id_inventaris di tabel detail_peminjaman yang id_peminjaman $id
		//id_inventaris ini akan digunakan sebagai paramater untuk memanggil data-data yang ada di tabel inventaris
		$dataD = DetailPeminjaman::
		where('id_peminjaman',$id)
		->first();
		
		//3. memanggil data inventaris seperti stok, yang id nya adalah id_inventaris(yang didapat dari detail_peminjaman)
		$dataI = Inventaris::
		where('id',$dataD->id_inventaris)
		->first();
		
		//4. membuat variabel stok akhir yaitu stok setelah barang dikembalikan rumusnya stok barang - jumlah
		$stok_akhir = $dataI->stok + $dataD->jumlah;
		
		//5. merubah stok menjadi nilai variabel stok akhir
		$update=Inventaris::find($dataI->id);
		$update->stok = $stok_akhir;
		$update->save();
		return redirect('peminjaman');
	}
}
