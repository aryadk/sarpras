<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventaris extends Model
{
    protected $table = 'inventaris';
	protected $fillable =[
	'nama','kode','kondisi','keterangan','stok','id_jenis','id_ruangan','tanggal_register',
	];
}
