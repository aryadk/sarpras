@extends('layouts.master')

@section('title','Laporan')
@section('konten')
<div class="container">
	<h2 class="text-center">Laporan Inventaris per Tanggal {{$tgl}}</h2><br>
	<table class="table">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Kondisi</th>
			<th>Stok</th>
			<th>Jenis</th>
			<th>Ruangan</th>
			<th>Tanggal Register</th>
		</tr>
	</thead>
	<tbody>
	@foreach($data as $datas)
		<tr>
			<td>{{$no++}}</td>
			<td>{{$datas->kode}}</td>
			<td>{{$datas->nama}}</td>
			<td>{{$datas->kondisi}}</td>
			<td>{{$datas->stok}}</td>
			<td>{{$datas->jenis}}</td>
			<td>{{$datas->ruangan}}</td>
			<td>{{$datas->tanggal_register}}</td>
		</tr>
	@endforeach
	</tbody>
	</table>
</div>
@endsection