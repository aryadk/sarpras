@extends('layouts.master2')

@section('title','Form Peminjaman')
@section('konten')
<div class="container ">
<h2>Login</h2><br>
	<form action="" method="">
		<div class="form-group">
			<label>Username</label>
			<input type="text" name="usrname" class="form-control">
		</div>
		<div class="form-group">
			<label>Password</label>
			<input type="text" name="pswd" class="form-control" >
		</div>
		<button class="btn btn-success btn-lg col-sm-12" type="submit">Login</button>
	</form>
</div>
@endsection