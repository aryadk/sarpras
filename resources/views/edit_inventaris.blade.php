@extends('layouts.master')

@section('title','Form Edit Inventaris')
@section('konten')
<div class="container">
<h2>Edit Barang</h2><br>
	<form action="{{url($data->id.'/edit_inventaris_log')}}" method="POST" class="col-sm-6">
	@csrf
		<div class="form-group">
			<label>Kode</label>
			<input type="text" name="kode" class="form-control" value="{{$data->kode}}">
		</div>
		<div class="form-group">
			<label>Nama Barang</label>
			<input type="text" name="nama_barang" class="form-control" value="{{$data->nama}}">
		</div>
		<div class="form-group">
			<label>Kondisi</label>
			<input type="text" name="kondisi" class="form-control" value="{{$data->kondisi}}">
		</div>
		<div class="form-group">
			<label>Keterangan</label>
			<textarea name="keterangan" class="form-control">{{$data->keterangan}}</textarea>
		</div>
		<div class="form-group">
			<label>Stok</label>
			<input type="number" name="stok" class="form-control" value="{{$data->stok}}">
		</div>
		<div class="form-group">
			<label>Jenis</label>
			<select name="jenis" class="form-control">
				<option value="{{$data->id_jenis}}">Default</option>
				@foreach($data1 as $data1)
				<option value="{{$data1->id}}">{{$data1->id}}. {{$data1->keterangan}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Ruangan</label>
			<select name="ruangan" class="form-control">
			<option value="{{$data->id_ruangan}}">Default</option>
				@foreach($data2 as $data2)
				<option value="{{$data2->id}}">{{$data2->id}}. {{$data2->kode_ruang}}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-success" type="submit">Simpan</button>
	</form>
</div>
@endsection