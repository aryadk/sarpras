@extends('layouts.master')

@section('title','Peminjaman')
@section('konten')
@if (session('alert'))
    <div class="alert alert-danger">
        {{ session('alert') }}
    </div>
@endif
<div class="container">
	<h2>Data Peminjaman</h2><br>
	<div class=row>
	<a href="{{url('/tambah_peminjaman_view')}}" class="col-sm-6"><button class="btn btn-success">+Tambah</button></a>
	</div>
	<table class="table table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>ID</th>
			<th>NIP</th>
			<th>Nama</th>
			<th>Nama Barang</th>
			<th>Jumlah</th>
			<th>Tanggal Pinjam</th>
			<th>Tanggal Kembali</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	@foreach($data as $data)
		<tr>
			<td>{{$no++}}</td>
			<td>{{$data->id}}</td>
			<td>{{$data->nip}}</td>
			<td>{{$data->nama}}</td>
			<td>{{$data->nama_barang}}</td>
			<td>{{$data->jumlah}}</td>
			<td>{{$data->tanggal_pinjam}}</td>
			<td>{{$data->tanggal_kembali}}</td>
			<td>{{$data->status}}</td>
			<td>@if($data->status == 'belum kembali')
				<a href="{{url('/sudah_kembali/'.$data->id)}}">Sudah dikembalikan</a>
				@else
				<p class="text-muted">Sudah dikembalikan</p>
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
	</table>
</div>
@endsection