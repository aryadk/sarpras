<html>
	<head>
		<title>@yield('title')</title>
		<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
		
	</head>
	<body>
		@yield('konten')
	</body>

</html>