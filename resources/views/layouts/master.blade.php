<html>
	<head>
		<title>@yield('title')</title>
		<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
		
	</head>
	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">SARPRAS</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="{{url('beranda')}}">Beranda</a></li>
					<li><a href="{{url('inventaris')}}">Data Inventaris</a></li>
					<li><a href="{{url('peminjaman')}}">Data Peminjaman</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{url('logout')}}">Logout</a></li>
				</ul>
			</div>
		</nav>
		@yield('konten')
	</body>

</html>