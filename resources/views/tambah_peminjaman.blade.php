@extends('layouts.master')

@section('title','Form Peminjaman')
@section('konten')
<div class="container">
<h2>Tambah Peminjaman</h2><br>
	<form action="{{url('/tambah_peminjaman_log')}}" method="POST">
	@csrf
		<div class="form-group">
			<label>*NIP</label>
			<input type="text" name="nip" class="form-control" required>
		</div>
		<div class="form-group">
			<label>*Nama</label>
			<input type="text" name="nama" class="form-control" required>
		</div>
		<div class="form-group">
			<label>*Nama Barang</label>
			<input type="text" name="nama_barang" class="form-control" required>
		</div>
		<div class="form-group">
			<label>*Jumlah</label>
			<input type="number" name="jumlah" class="form-control" required>
		</div>
		<button class="btn btn-success" type="submit">Simpan</button>
	</form>
</div>
@endsection