@extends('layouts.master')

@section('title','Form Inventaris')
@section('konten')
<div class="container">
<h2>Tambah Barang</h2><br>
	<form action="{{url('tambah_inv_log')}}" method="POST" class="col-sm-6">
	@csrf
		<div class="form-group">
			<label>Kode</label>
			<input type="text" name="kode" class="form-control">
		</div>
		<div class="form-group">
			<label>Nama Barang</label>
			<input type="text" name="nama_barang" class="form-control">
		</div>
		<div class="form-group">
			<label>Kondisi</label><br>
			<input type="radio" name="kondisi" value="baik">Baik
			<input type="radio" name="kondisi" value="rusak">Rusak
		</div>
		<div class="form-group">
			<label>Keterangan</label>
			<textarea name="keterangan" class="form-control"></textarea>
		</div>
		<div class="form-group">
			<label>Stok</label>
			<input type="number" name="stok" class="form-control">
		</div>
		<div class="form-group">
			<label>Jenis</label>
			<select name="jenis" class="form-control">
				<option></option>
				@foreach($data as $data)
				<option value="{{$data->id}}">{{$data->id}}. {{$data->keterangan}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Ruangan</label>
			<select name="ruangan" class="form-control">
				<option></option>
				@foreach($data1 as $data1)
				<option value="{{$data1->id}}">{{$data1->id}}. {{$data1->kode_ruang}}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-success" type="submit">Simpan</button>
	</form>
</div>
@endsection