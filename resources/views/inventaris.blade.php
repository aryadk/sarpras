@extends('layouts.master')

@section('title','Inventaris')
@section('konten')
<div class="container">
	<h2>Data Inventaris</h2><br>
	<div class=row>
	<a href="{{url('/tambah_inventaris')}}" class="col-sm-6"><button class="btn btn-success">+Tambah</button></a>
	<a href="{{url('/laporan')}}" class="col-sm-6 text-right"><button class="btn btn-danger">Buat Laporan</button></a>
	</div>
	<table class="table table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Kondisi</th>
			<th>Stok</th>
			<th>Jenis</th>
			<th>Ruangan</th>
			<th>Tanggal Register</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	@foreach($data as $datas)
		<tr>
			<td>{{$no++}}</td>
			<td>{{$datas->kode}}</td>
			<td>{{$datas->nama}}</td>
			<td>{{$datas->kondisi}}</td>
			<td>{{$datas->stok}}</td>
			<td>{{$datas->jenis}}</td>
			<td>{{$datas->ruangan}}</td>
			<td>{{$datas->tanggal_register}}</td>
			<td><a href="{{url('/edit_inventaris/'.$datas->id)}}">Edit</a> | <a href="{{url('/tambah_stok/'.$datas->id)}}">+</a>
			<a href="{{url('/kurang_stok/'.$datas->id)}}">-</a></td>
		</tr>
	@endforeach
	</tbody>
	</table>
</div>
@endsection