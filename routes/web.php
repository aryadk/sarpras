<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/beranda', function () {
    return view('beranda');
});
Route::get('/inventaris', 'inventarisController@index');
Route::get('/tambah_stok/{id}', 'inventarisController@tambah_stok');
Route::get('/kurang_stok/{id}', 'inventarisController@kurang_stok');
Route::get('/tambah_inventaris','inventarisController@tambahView');
Route::post('/tambah_inv_log','inventarisController@tambah');
Route::get('/edit_inventaris/{id}', 'inventarisController@editView');
Route::post('{id}/edit_inventaris_log','inventarisController@edit');
Route::get('/laporan', 'inventarisController@laporan');
Route::get('/peminjaman','peminjamanController@index');
Route::get('/sudah_kembali/{id}','peminjamanController@sudah_kembali');
Route::get('/tambah_peminjaman_view','peminjamanController@tambahView');
Route::post('/tambah_peminjaman_log','peminjamanController@tambah');
Route::get('/loginhome', function () {
    return view('loginhome');
});
Route::get('/login', function () {
    return view('login');
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
